# Host Switcher

A simple utility to swap active host files locally.
It was originally intended as a utility to save me time during development, and hasn't received updates since.

1.	Clone repo and build.

    clone https://ness2u@bitbucket.org/ness2u/hostswitcher.git  
    cd hostswitcher && msbuild 

2.	Backup existing host file; hosts.original

3.	Copy host files to C:\Windows\System32\drivers\etc directory.
	Follow a meaningful naming convention; the utility will pickup hosts.<name> files.

4.	Run HostSwitcher.exe with admin permissions.

5.	While I have not had the problem, you may need to flush dns after switching host files.

    ipconfig --flushdns