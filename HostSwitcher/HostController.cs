﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace HostSwitcher
{
    public class HostController
    {
        const string CurrentHostFile = @"C:\Windows\System32\drivers\etc\hosts";
        const string LastHostFile = @"C:\Windows\System32\drivers\etc\hosts.last";
        const string HostFilePath = @"C:\Windows\System32\drivers\etc";

        private static readonly Regex FileMask = new Regex(@".*?\\hosts\.(?<name>[^\.]+)(?:\.txt)?",
            RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);

        private static void ForceNotReadonly(string path)
        {
            var fileInfo = new FileInfo(path);
            fileInfo.IsReadOnly = false;
        }

        public IEnumerable<HostFile> HostFiles
        {
            get
            {
                var files = Directory.GetFiles(HostFilePath)
                    .Select(f => FileMask.Match(f))
                    .Where(m => m.Success)
                    .Select(m => new HostFile()
                    {
                        Name = m.Groups["name"].Value,
                        Path = m.Captures[0].Value
                    })
                    .ToList();
                return files;
            }
        }

        public bool UseHostFile(HostFile file)
        {
            try
            {
                if (File.Exists(CurrentHostFile))
                {
                    if (File.Exists(LastHostFile))
                    {
                        ForceNotReadonly(LastHostFile);
                        File.Delete(LastHostFile);
                    }
                    File.Move(CurrentHostFile, LastHostFile);
                }
                File.Copy(file.Path, CurrentHostFile);
                ForceNotReadonly(CurrentHostFile);
                return true;
            }
            catch (Exception exc)
            {
                return false;
            }
        }
    }

    public class HostFile
    {
        public string Name { get; set; }
        public string Path { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
