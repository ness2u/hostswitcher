﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HostSwitcher
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly HostController _controller = new HostController();

        public MainWindow()
        {
            InitializeComponent();

            lstHosts.ItemsSource = _controller.HostFiles;
        }

        private void HostFile_Selected(object sender, RoutedEventArgs e)
        {
            var item = lstHosts.SelectedItem as HostFile;
            if (item != null)
            {
                var success = _controller.UseHostFile(item);
                MessageBox.Show(success ? "Success" : "Unable to change host, run as admin.");
            }
            else
            {
                MessageBox.Show("Select a host first...");
            }
        }


    }
}
